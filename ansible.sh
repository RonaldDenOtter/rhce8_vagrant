#!/bin/bash

echo "[TASK 1] Install packages"
yum -y install epel-release

echo "[TASK 2] Add ansible user and reset password"
useradd ansible
echo "ansible:vagrant" | chpasswd

echo "[TASK 3] Setup privilege escallation"
echo "ansible ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/ansible

echo "[TASK 4] Modify /etc/hosts"
sed "s/^192.168.4.*/d" /etc/hosts
echo "192.168.4.200 control.example.com control" >> /etc/hosts
echo "192.168.4.201 ansible1.example.com ansible1" >> /etc/hosts
echo "192.168.4.202 ansible2.example.com ansible2" >> /etc/hosts

exit 0
