#!/bin/bash
echo "[TASK 1] Install required packages"
yum install -y epel-release python3 vim
yum install -y sshpass
yum install -y ansible

echo "[TASK 2] Modify /etc/hosts"
sed -i "/^192.168.4.*/d" /etc/hosts
echo "192.168.4.200 control.example.com control" >> /etc/hosts
echo "192.168.4.201 ansible1.example.com ansible1" >> /etc/hosts
echo "192.168.4.202 ansible2.example.com ansible2" >> /etc/hosts

echo "[TASK 3] Create ansible exercise user and configure ssh keys"
adduser ansible
echo "ansible ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/ansible
echo "ansible:vagrant" | chpasswd
su - ansible << "EOF"
mkdir -vp /home/ansible/.ssh
cd /home/ansible/.ssh 
ssh-keygen -N "" -f id_rsa # Generate public and private key pairs (id_rsa, id_rsa.pub)
# Copy SSH Keys
sshpass -p "vagrant" ssh-copy-id -o StrictHostKeyChecking=no -i id_rsa.pub ansible@ansible1.example.com
sshpass -p "vagrant" ssh-copy-id -o StrictHostKeyChecking=no -i id_rsa.pub ansible@ansible2.example.com
EOF
